const {
  addClassroom,
  getAllClassroom,
  deleteClassroom,
  modifyClassroom,
} = require("../controllers/classroomController");

const router = require("express").Router();

router.post("/", addClassroom);
router.get("/", getAllClassroom);
router.put("/:id", modifyClassroom);
router.delete("/:id", deleteClassroom);

module.exports = router;
