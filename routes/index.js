const router = require("express").Router();
const classroomRoute = require("./classroomRoute");

router.use(
  "/classroom",
  classroomRoute
);

module.exports = router;
