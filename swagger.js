const swaggerAutogen = require("swagger-autogen")();

const docs = {
  info: {
    title: "API",
    version: "1.0.0",
    description: "This is the API for the application",
  },

  host: "localhost:3000",
  basePath: "/",
  securityDefinitions: {
    Bearer: {
      type: "apiKey",
      name: "Authorization",
      scheme: "bearer",
      in: "header",
    },
  },
  definitions: {
    classroom: {
      $building: "A",
      active: true,
    },
  },
  security: [{ Bearer: [] }],
};

const outputFile = "./swagger_output.json";
const endpointsFiles = ["./routes/index.js"];

swaggerAutogen(outputFile, endpointsFiles, docs);
