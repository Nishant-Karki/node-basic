"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addConstraint("accounts", {
      fields: ["studentId"],
      type: "foreign key",
      name: "account_student_associate",
      references: {
        table: "students",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("accounts", {
      fields: ["staffId"],
      type: "foreign key",
      name: "account_staff_associate",
      references: {
        table: "staffs",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("students", {
      fields: ["classroomId"],
      type: "foreign key",
      name: "classroom_student_associate",
      references: {
        table: "classrooms",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("students", {
      fields: ["subjectId"],
      type: "foreign key",
      name: "subject_student_associate",
      references: {
        table: "subjects",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });

    await queryInterface.addConstraint("staffs", {
      fields: ["classroomId"],
      type: "foreign key",
      name: "classroom_staff_associate",
      references: {
        table: "classrooms",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("staffs", {
      fields: ["subjectId"],
      type: "foreign key",
      name: "subject_staff_associate",
      references: {
        table: "subjects",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("exams", {
      fields: ["resultId"],
      type: "foreign key",
      name: "exam_result_associate",
      references: {
        table: "results",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.addConstraint("subjects", {
      fields: ["examId"],
      type: "foreign key",
      name: "exam_subject_associate",
      references: {
        table: "exams",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeConstraint("accounts", {
      fields: ["studentId"],
      type: "foreign key",
      name: "account_student_associate",
      references: {
        table: "students",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("accounts", {
      fields: ["staffId"],
      type: "foreign key",
      name: "account_staff_associate",
      references: {
        table: "staffs",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("students", {
      fields: ["classroomId"],
      type: "foreign key",
      name: "classroom_student_associate",
      references: {
        table: "classrooms",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("students", {
      fields: ["subjectId"],
      type: "foreign key",
      name: "subject_student_associate",
      references: {
        table: "subjects",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("staffs", {
      fields: ["classroomId"],
      type: "foreign key",
      name: "classroom_staff_associate",
      references: {
        table: "classrooms",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("staffs", {
      fields: ["subjectId"],
      type: "foreign key",
      name: "subject_staff_associate",
      references: {
        table: "subjects",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("exams", {
      fields: ["resultId"],
      type: "foreign key",
      name: "exam_result_associate",
      references: {
        table: "results",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
    await queryInterface.removeConstraint("subjects", {
      fields: ["examId"],
      type: "foreign key",
      name: "exam_subject_associate",
      references: {
        table: "exams",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },
};
