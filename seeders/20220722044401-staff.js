"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "staffs",
      [
        {
          firstName: "Kaizoku",
          lastname: "Nish",
          email: "kaizoku.nish@gmail.com",
          phone: 9840209779,
          address: "Bhaktapur",
          type: "Teacher",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          firstName: "John",
          lastname: "Doe",
          email: "john.doe@gmail.com",
          phone: 9840209999,
          address: "Lagankhel",
          type: "Accountant",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          firstName: "Mary",
          lastname: "Doe",
          email: "mary.doe@gmail.com",
          phone: 9840209999,
          address: "Balaju",
          type: "Teacher",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],

      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("staffs", null, {});
  },
};
