"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class exam extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.result, {
        foreignKey: { name: "resultId", allowNull: false },
        onDelete: "CASCADE",
      });
      models.result.hasOne(this);
    }
  }
  exam.init(
    {
      date: DataTypes.DATE,
      hours: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "exam",
    }
  );
  return exam;
};
