"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class subject extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.exam, {
        foreignKey: { name: "examId", allowNull: false },
        onDelete: "CASCADE",
      });
      models.exam.hasOne(this);
    }
  }
  subject.init(
    {
      name: DataTypes.STRING,
      credits: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "subject",
    }
  );
  return subject;
};
