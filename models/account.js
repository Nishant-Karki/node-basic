"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.student, {
        foreignKey: { name: "studentId", allowNull: false },
        onDelete: "CASCADE",
      });
      models.student.hasOne(this);

      this.hasMany(models.staff, {
        foreignKey: { name: "staffId", allowNull: false },
        onDelete: "CASCADE",
      });
      models.staff.hasOne(this);
    }
  }
  account.init(
    {
      type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "account",
    }
  );
  return account;
};
