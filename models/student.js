"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.belongsTo(models.classroom, {
        foreignKey: { name: "classroomId", allowNull: false },
        onDelete: "CASCADE",
      });
      models.classroom.hasMany(this);

      this.hasMany(models.subject, {
        foreignKey: { name: "subjectId", allowNull: false },
        onDelete: "CASCADE",
      });
      models.subject.hasMany(this);
    }
  }
  student.init(
    {
      firstName: DataTypes.STRING,
      lastname: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "student",
    }
  );
  return student;
};
