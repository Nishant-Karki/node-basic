const { Classroom } = require("../models");

exports.createClassroom = async (params) => {
  return await Classroom.create(params);
};

exports.readAllClassroom = async () => {
  return await Classroom.findAll();
};

exports.updateClassroom = async (id, params) => {
  return await Classroom.update({ ...params }, { where: { id } });
};

exports.removeClassroom = async (id) => {
  return await Classroom.destroy({ where: { id } });
};
