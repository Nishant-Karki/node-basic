const {
  createClassroom,
  readAllClassroom,
  removeClassroom,
  updateClassroom,
} = require("../services/classroomService");

exports.addClassroom = async (req, res) => {
  // #swagger.tags =['Classroom Controller']'
  // #swagger.description = 'Api to get add classroom'
  // #swagger.summary = 'Api to get add classroom'
  /*  #swagger.parameters['addClassroom'] = {
                in: 'body',
                description: 'Api to get add classroom',
                schema: {
                   $ref:'#/definitions/classroom'
                }
        } */
  try {
    await createClassroom(req.body);
    res.status(200).json("Classroom Created");
  } catch (err) {
    res.status(500).json(err.message);
  }
};
exports.getAllClassroom = async (req, res) => {
  // #swagger.tags =['Classroom Controller']'
  // #swagger.summary = 'Get all classroom information'
  /* #swagger.responses[200] = {
                description: 'Success',
                schema: {
                    building: 'A',
                    active: true
                }
        } */

  /* #swagger.responses[401] = {
                description: 'Unathorized',
                schema: {
                   error: "The request is unauthorized"
                }
        } */

  try {
    const data = await readAllClassroom();
    res.status(200).json(data);
  } catch (err) {
    res.status(401);
    res.status(500).json(err.message);
  }
};

exports.modifyClassroom = async (req, res) => {
  // #swagger.tags =['Classroom Controller']'
  // #swagger.summary = 'Update classroom information'

  try {
    const { building, active } = req.body;
    await updateClassroom(req.params.id, req.body);
    res.status(200).json("Classroom updated");
  } catch (err) {
    res.status(404).json("NOT FOUND");
    res.status(500).json(err.message);
  }
};

exports.deleteClassroom = async (req, res) => {
  // #swagger.tags =['Classroom Controller']'
  // #swagger.summary = 'Delete classroom information'

  try {
    await removeClassroom(req.params.id);
    res.status(200).json("Classroom deleted");
  } catch (err) {
    res.status(500).json(err.message);
  }
};
