const express = require("express");
const app = express();
const cors = require("cors");
const db = require("./models");
const indexRoute = require("./routes");
const swaggerui = require("swagger-ui-express");
const swaggerdocs = require("./swagger_output.json");

app.use("/api-docs", swaggerui.serve, swaggerui.setup(swaggerdocs));

(async () => {
  try {
    await db.sequelize.authenticate();
    // await db.sequelize.sync();
    console.log("Database created successfully");
  } catch (e) {
    console.log(e);
  }
})();

app.use(cors());
app.use(express.json());

app.use(indexRoute);

app.listen(3000, (err) => {
  if (err) throw err;
  console.log("Server Started");
});
